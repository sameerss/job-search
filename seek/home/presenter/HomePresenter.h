//
//  HomePresenter.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UINavigationController.h>
#import "SearchRequest.h"

@interface HomePresenter : NSObject

-(void) searchJobRequest:(SearchRequest *)searchRequest fromViewController:(UIViewController *)viewController;

@end
