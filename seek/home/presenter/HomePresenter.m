//
//  HomePresenter.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "HomePresenter.h"
#import "HomeRouter.h"

@implementation HomePresenter

-(id) init {
    self = [super init];
    if(self) {
        return self;
    }
    return nil;
}

/*!
 * @discussion Passes the search request to the router to push SearchViewController
 * @param searchRequest SearchRequest Object containing the search parameters (keywords)
 * @param viewController UIViewController which is making this request
 */
-(void) searchJobRequest:(SearchRequest *)searchRequest fromViewController:(UIViewController *)viewController {
    HomeRouter *router = [[HomeRouter alloc] init];
    [router pushSearchViewControllerOn:viewController withRequest:searchRequest];
}

@end
