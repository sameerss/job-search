//
//  HomeRouter.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchRequest.h"
#import "UIKit/UIViewController.h"

@interface HomeRouter : NSObject

-(void) pushSearchViewControllerOn: (UIViewController *) viewController withRequest:(SearchRequest *) searchRequest;

@end
