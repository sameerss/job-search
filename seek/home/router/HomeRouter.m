//
//  HomeRouter.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "HomeRouter.h"
#import "SearchViewController.h"
#import "SearchInteractor.h"
#import "SearchPresenter.h"

@implementation HomeRouter

/*!
 * @discussion Responsible for setting up SearchVC with interactor,presenter and searchRequest and pushing it on the navigationController
 * @param viewController UIViewController which is making this request
 * @param searchRequest SearchRequest Object containing the search parameters (keywords)
 */

-(void) pushSearchViewControllerOn: (UIViewController *) viewController withRequest:(SearchRequest *) searchRequest{
    SearchInteractor *interactor = [[SearchInteractor alloc] init];
    SearchPresenter *presenter = [[SearchPresenter alloc] init];
    SearchViewController *searchVC = [[SearchViewController alloc] init];
    
    presenter.interactor = interactor;
    presenter.jobSearchView = searchVC;
    interactor.presenter = presenter;
    
    searchVC.searchRequest = searchRequest;
    searchVC.presenter = presenter;
    
    [viewController.navigationController pushViewController:searchVC animated:YES];
}

@end
