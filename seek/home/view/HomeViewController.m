//
//  HomeViewController.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "HomeViewController.h"
#import "SearchRequest.h"
#import "HomePresenter.h"
#import "Utils.h"

@interface HomeViewController ()

@property (nonatomic, strong) HomePresenter *presenter;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Job Search";
    [self.userMessageTF setHidden:YES];
    if(!self.presenter) {
        self.presenter = [[HomePresenter alloc] init];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
}


- (IBAction)searchTapped:(id)sender {
    if([Utils isOnline]) {
        [self.userMessageTF setHidden:YES];
        NSString *searchText = self.searchTextField.text;
        SearchRequest *searchReq = [[SearchRequest alloc] initWithSearchText:searchText];
        [self.presenter searchJobRequest:searchReq fromViewController:self];
    } else {
        [self.userMessageTF setHidden:NO];
    }
}


@end
