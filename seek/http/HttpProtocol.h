//
//  HttpProtocol.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HttpRequestSuccess) (NSDictionary *theData);
typedef void (^HttpRequestFailure) (NSError *theError);

@protocol HttpProtocol <NSObject>

-(void)post:(NSString *)url postData:(NSDictionary *)postData onSuccess:(HttpRequestSuccess)completion onFailure:(HttpRequestFailure)failure;

-(void) get:(NSString *)url onSuccess:(HttpRequestSuccess)completion onFailure:(HttpRequestFailure)failure;

-(BOOL) isOnline;

@end
