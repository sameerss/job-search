//
//  HttpRequestManager.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpProtocol.h"
#import "Reachability.h"

@interface HttpRequestManager : NSObject <NSURLConnectionDelegate,HttpProtocol>

@property (strong,nonatomic) HttpRequestSuccess httpSuccess;
@property (strong,nonatomic) HttpRequestFailure httpFailure;
@property (nonatomic,strong) NSHTTPURLResponse *httpUrlResponse;
@property (nonatomic,strong) NSDictionary *responseDictionary;

@end
