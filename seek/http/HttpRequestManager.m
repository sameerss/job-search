//
//  HttpRequestManager.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "HttpRequestManager.h"

@interface HttpRequestManager ()

@property (nonatomic,strong) NSURLSession *session;

@end

@implementation HttpRequestManager

-(id) init
{
    self = [super init];
    if (self) {
        self.session = [self configuredSession];
    }
    return self;
}

/*!
 * @discussion Passes the search request to the interactor which will initate search based on business rules & domain
 * @return configuredSession NSURLSession Object needed for making Http calls
 */
-(NSURLSession*) configuredSession
{
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.allowsCellularAccess = YES;
    
    sessionConfig.timeoutIntervalForRequest = 30.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    
    return [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
}

-(NSURLSession *) defaultSession
{
    return [NSURLSession sharedSession];
}

/*!
 * @discussion Determines if the internet is reachable
 * @return BOOL YES if internet is reachable, NO if not
 */
-(BOOL) isOnline
{
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    return YES;
}


/*!
 * @discussion Makes a http get call to the passed url
 * @param url NSString which specific the url to fetch via the HTTP GET method
 * @param completion Block called on successful Http request
 * @param failure Block called on failed Http request
 */
-(void) get:(NSString *)url onSuccess:(HttpRequestSuccess)completion onFailure:(HttpRequestFailure)failure
{
    if(self.session == nil) {
        self.session = [self configuredSession];
    }
    self.httpSuccess = completion;
    self.httpFailure = failure;
    
    NSLog(@"HHTP GET: %@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    NSURLSessionDataTask * dataTask = [self.session dataTaskWithRequest:request
                                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                          [self parseResponseData:data withResponse:response withError:error];
                                                      }];
    [dataTask resume];
    
}

/*!
 * @discussion Makes a http post request to the passed url with the dictionary as post data
 * @param url NSString which specific the url to fetch via the HTTP POST method
 * @param completion Block called on successful Http request
 * @param failure Block called on failed Http request
 */
-(void) post:(NSString *)url postData:(NSDictionary *)postDictionary onSuccess:(HttpRequestSuccess)completion onFailure:(HttpRequestFailure)failure
{
    if(self.session == nil) {
        self.session = [self configuredSession];
    }
    self.httpSuccess = completion;
    self.httpFailure = failure;
    
    NSString *post = [self makeParamtersString:[postDictionary mutableCopy] withEncoding:NSUTF8StringEncoding];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask * dataTask = [self.session dataTaskWithRequest:request
                                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                          [self parseResponseData:data withResponse:response withError:error];
                                                      }];
    [dataTask resume];
}

/*!
 * @discussion Converts the dictionary into a string with key=value format for URL
 * @param parameters NSString which specific the url to fetch via the HTTP GET method
 * @param encoding Block called on successful Http request
 * @return String in key=value format
 */
- (NSString*)makeParamtersString:(NSMutableDictionary*)parameters withEncoding:(NSStringEncoding)encoding
{
    if (nil == parameters || [parameters count] == 0)
        return nil;
    
    NSMutableString* stringOfParamters = [[NSMutableString alloc] init];
    NSEnumerator *keyEnumerator = [parameters keyEnumerator];
    id key = nil;
    while ((key = [keyEnumerator nextObject]))
    {
        NSString *value = [[parameters valueForKey:key] isKindOfClass:[NSString class]] ?
        [parameters valueForKey:key] : [[parameters valueForKey:key] stringValue];
        [stringOfParamters appendFormat:@"%@=%@&",
         key,value];
    }
    
    // Delete last character of '&'
    NSRange lastCharRange = {[stringOfParamters length] - 1, 1};
    [stringOfParamters deleteCharactersInRange:lastCharRange];
    return stringOfParamters;
}

/*!
 * @discussion parses the response NSData from JSON into NSDictionary and calls the success completion block
 * @param data NSData raw response data
 * @param response NSURLResponse object
 * @param error NSError error object
 */
-(void) parseResponseData:(NSData *)data withResponse:(NSURLResponse *)response withError:(NSError *) error {
    
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *) response;
    
    self.httpUrlResponse = urlResponse;
    
    //NSLog(@"RESPONSE DATA: %ld %@",(long)self.httpUrlResponse.statusCode,[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

    if(!error) {
        NSDictionary *responseDict = nil;
        
        if(self.httpUrlResponse.statusCode >= 200 && self.httpUrlResponse.statusCode <= 299) {
            if(data) {
                NSError *dataError = nil;
                NSObject *responseObj =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&dataError];
                
                if([responseObj isKindOfClass:[NSArray class]]) {
                    responseDict = [NSDictionary dictionaryWithObject:(NSArray *)responseObj forKey:@"data"];
                }
                else if([responseObj isKindOfClass:[NSDictionary class]]) {
                    responseDict = (NSDictionary *)responseObj;
                }
            }

            self.responseDictionary = responseDict;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.httpSuccess(self.responseDictionary);
            });
        }
        else {
            //Non 200 HTTP response

            if (response != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.httpFailure([NSError errorWithDomain:@"HttpError" code:self.httpUrlResponse.statusCode userInfo:@{NSLocalizedDescriptionKey: response }]);
                });
            }
            else{
                // Common problem
                NSMutableDictionary *responseDummy = [[NSMutableDictionary alloc] init];
                [responseDummy setValue:@"Unexpected error. Try again" forKey:@"message"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.httpFailure([NSError errorWithDomain:@"MBHttpError" code:self.httpUrlResponse.statusCode userInfo:@{NSLocalizedDescriptionKey: responseDummy}]);
                });
            }
        }
    }
    else {
        NSMutableDictionary *responseDummy = [[NSMutableDictionary alloc] init];
        [responseDummy setValue:@"Unexpected error. Try again" forKey:@"message"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.httpFailure([NSError errorWithDomain:@"HttpError" code:self.httpUrlResponse.statusCode userInfo:@{NSLocalizedDescriptionKey: responseDummy}]);
        });
    }
    
}

@end
