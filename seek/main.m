//
//  main.m
//  seek
//
//  Created by Sameer Sangawar on 22/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
