//
//  ServiceProvider.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpProtocol.h"
#import "HttpRequestManager.h"
#import "SearchProtocol.h"
#import "SearchPresenter.h"
#import "SearchInteractor.h"

@interface ServiceProvider : NSObject

+(id<HttpProtocol>) http;

@end
