//
//  SearchProtocol.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchJobsRequest.h"
#import "SearchJobResponse.h"
#import "SearchRequest.h"


@protocol SearchInteractorInput <NSObject>
-(void) searchJobs: (SearchJobsRequest *)searchJobReq;
@end

@protocol SearchInteractorOutput <NSObject>
-(void) jobSearchResponse: (SearchJobResponse *)searchJobResp;
@end

@protocol SearchListView <NSObject>
-(void) receivedSearchResults: (SearchJobResponse *)searchJobResp;
@end

@protocol SearchPresenterInput <NSObject>
-(void) searchWithRequest: (SearchRequest *)searchRequest;
@end








