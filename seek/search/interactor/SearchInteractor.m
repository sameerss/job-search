//
//  SearchInteractor.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchInteractor.h"
#import "ServiceProvider.h"
#import "Job.h"
#import "SearchJobResponse.h"
#import "Utils.h"

@implementation SearchInteractor

-(id) init {
    self = [super init];
    if(self) {
        return self;
    }
    return nil;
}

-(void) searchJobs:(SearchJobsRequest *)searchJobReq {
    
    id<HttpProtocol> httpProvider = [ServiceProvider http];
    
    NSString *keywords = [searchJobReq.Keywords stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    
    NSString *highlight = @"false";
    if(searchJobReq.highlight) {
        highlight = @"true";
    }
    
    NSString *baseUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"base_url"];
    
    NSString *url = [[NSString alloc] initWithFormat:@"%@search?Keywords=%@&Location=%@&Page=%d&PageSize=%d&Salary=%@&SalaryRange=%@&SalaryType=%@&SiteKey=%@&highlight=%@",baseUrl,
                     keywords,searchJobReq.Location,searchJobReq.Page,searchJobReq.PageSize,searchJobReq.Salary,searchJobReq.SalaryRange, searchJobReq.SalarayType,searchJobReq.SiteKey,highlight];
    
    [httpProvider get:url onSuccess:^(NSDictionary *data) {
        [self informPresenter:[self parseSearchJobsResponse:data]];
    } onFailure:^(NSError *error) {
        //Complete Error handling can be done here. Currently returning an empty result
        SearchJobResponse *searchResponse = [[SearchJobResponse alloc] init];
        [self informPresenter: searchResponse];
    }];
    
}

-(SearchJobResponse *) parseSearchJobsResponse: (NSDictionary *)data {
    
    SearchJobResponse *searchResponse = [[SearchJobResponse alloc] init];
    NSMutableArray *jobs = [[NSMutableArray alloc] init];
    
    NSArray *jobData = [data objectForKey:@"data"];
    if(jobData != nil) {
        int jobCount = (int)jobData.count;
        for(int i = 0; i < jobCount; i++) {
            NSDictionary *jobDict = [jobData objectAtIndex:i];
            Job *job = [[Job alloc] init];
            
            if(nil != [jobDict objectForKey:@"id"] && [NSNull null] != [jobDict objectForKey:@"id"]) {
                job.jobId = [[jobDict objectForKey:@"id"] longValue];
            }
            
            if(nil != [jobDict objectForKey:@"title"] && [NSNull null] != [jobDict objectForKey:@"title"]) {
                job.title = [jobDict objectForKey:@"title"];
            }
            
            if(nil != [jobDict objectForKey:@"listingDate"] && [NSNull null] != [jobDict objectForKey:@"listingDate"]) {
                job.listingDate = [Utils getDateFromDateString:[jobDict objectForKey:@"listingDate"] withFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
            }
            
            if(nil != [jobDict objectForKey:@"advertiser"] && [NSNull null] != [jobDict objectForKey:@"advertiser"]) {
                if(nil != [[jobDict objectForKey:@"advertiser"] objectForKey:@"description"] && [NSNull null] != [[jobDict objectForKey:@"advertiser"] objectForKey:@"description"]) {
                    job.advertiser = [[jobDict objectForKey:@"advertiser"] objectForKey:@"description"];
                }
            }
            
            if(nil != [jobDict objectForKey:@"teaser"] && [NSNull null] != [jobDict objectForKey:@"teaser"]) {
                job.summary = [jobDict objectForKey:@"teaser"];
            }
            
            [jobs addObject:job];
        }
        
        searchResponse.title = [data objectForKey:@"title"];
        searchResponse.jobCount = [[data objectForKey:@"totalCount"] intValue];
        searchResponse.jobs = jobs;
    }

    return searchResponse;
}

-(void) informPresenter: (SearchJobResponse *)searchResponse {
    [self.presenter jobSearchResponse:searchResponse];
}

@end
