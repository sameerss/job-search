//
//  Job.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject

@property (nonatomic) long jobId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDate *listingDate;
@property (nonatomic, strong) NSString *displayListingDate;
@property (nonatomic, strong) NSString *advertiser;
@property (nonatomic, strong) NSString *summary;

@end
