//
//  Job.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "Job.h"

@implementation Job

-(id) init {
    self = [super init];
    if(self) {
        self.jobId = -1;
        self.title = @"";
        self.listingDate = [[NSDate alloc] init];
        self.displayListingDate = @"";
        self.advertiser = @"";
        self.summary = @"";
        return self;
    }
    return nil;
}


@end
