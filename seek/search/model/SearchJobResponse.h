//
//  SearchJobResponse.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchJobResponse : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) int jobCount;
@property (nonatomic, strong) NSMutableArray *jobs;

@end
