//
//  SearchJobResponse.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchJobResponse.h"

@implementation SearchJobResponse

- (id) init {
    self = [super init];
    if(self) {
        self.title = @"";
        self.jobCount = 0;
        self.jobs = [[NSMutableArray alloc] init];
        return self;
    }
    return nil;
}

@end
