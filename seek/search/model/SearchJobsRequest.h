//
//  SearchJobsRequest.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchJobsRequest : NSObject

@property (nonatomic, strong) NSString* Keywords;
@property (nonatomic, strong) NSString* Location;
@property (nonatomic) int Page;
@property (nonatomic) int PageSize;
@property (nonatomic, strong) NSString* Salary;
@property (nonatomic, strong) NSString* SalaryRange;
@property (nonatomic, strong) NSString* SalarayType;
@property (nonatomic, strong) NSString* SiteKey;
@property (nonatomic) BOOL highlight;

-(id) initWithKeywords: (NSString *)keywords;

@end
