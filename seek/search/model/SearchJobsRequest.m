//
//  SearchJobsRequest.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchJobsRequest.h"

@implementation SearchJobsRequest

-(id) init {
    NSAssert(false,@"unavailable, use initWithKeywords: instead");
    return nil;
}


-(id) initWithKeywords: (NSString *)keywords {
    self = [super init];
    if(self) {
        self.Keywords = keywords;
        self.Location = @"3000";
        self.Page = 1;
        self.PageSize = 20;
        self.Salary = @"0-999999";
        self.SalaryRange = @"0-999999";
        self.SalarayType = @"Annual";
        self.SiteKey = @"au";
        self.highlight = false;
        
        return self;
    }
    return nil;
}

@end
