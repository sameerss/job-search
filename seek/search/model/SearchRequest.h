//
//  SearchRequest.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchRequest : NSObject

@property (nonatomic,strong) NSString* searchText;
@property (nonatomic) int page;

-(id) initWithSearchText: (NSString *)searchText;

@end
