//
//  SearchRequest.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchRequest.h"


@implementation SearchRequest

-(id) init {
    NSAssert(false,@"unavailable, use initWithSearchText: instead");
    return nil;
}

-(id) initWithSearchText: (NSString *)searchText {
    self = [super init];
    if(self) {
        self.searchText = searchText;
        self.page = 1;
        return self;
    }    
    return nil;
}

@end
