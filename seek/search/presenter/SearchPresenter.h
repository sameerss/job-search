//
//  SearchPresenter.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchProtocol.h"
#import "SearchRequest.h"
#import "SearchInteractor.h"
#import "SearchJobsRequest.h"

@interface SearchPresenter : NSObject <SearchPresenterInput,SearchInteractorOutput>

@property (nonatomic, strong) id<SearchInteractorInput> interactor;
@property (nonatomic, strong) id<SearchListView> jobSearchView;

@end
