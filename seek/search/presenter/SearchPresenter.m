//
//  SearchPresenter.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchPresenter.h"
#import "Job.h"
#import "Utils.h"

@implementation SearchPresenter

-(id) init {
    self = [super init];
    if(self) {
    	return self;
    }
    return nil;
}

/*!
 * @discussion Passes the search request to the interactor which will initiate search based on business rules & domain
 * @param searchRequest SearchRequest Object containing the search parameters (keywords)
 */
-(void) searchWithRequest: (SearchRequest *)searchRequest {
    SearchJobsRequest *searchJobsReq = [[SearchJobsRequest alloc] initWithKeywords:searchRequest.searchText];
    searchJobsReq.Page = searchRequest.page;
    [self.interactor searchJobs:searchJobsReq];
}

/*!
 * @discussion Delegate method called by interactor to pass the search results. These are modified for display.
 * @param searchJobResp SearchJobResponse Object containing the search parameters (keywords)
 */
-(void) jobSearchResponse:(SearchJobResponse *)searchJobResp {
    for(int i = 0; i < searchJobResp.jobs.count; i++) {
        Job *job = [searchJobResp.jobs objectAtIndex:i];
        job.displayListingDate = [Utils getDateStringFromDate:job.listingDate withFormat:@"d MMM"];
    }
    [self.jobSearchView receivedSearchResults:searchJobResp];
}
@end
