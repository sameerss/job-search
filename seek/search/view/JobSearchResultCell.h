//
//  JobSearchResultCellTableViewCell.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobSearchResultCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *jobTitle;
@property (weak, nonatomic) IBOutlet UILabel *jobAdvertiser;
@property (weak, nonatomic) IBOutlet UILabel *jobSummary;
@end
