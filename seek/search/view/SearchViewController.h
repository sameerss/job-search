//
//  SearchViewController.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchRequest.h"
#import "SearchProtocol.h"

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SearchListView>
@property (weak, nonatomic) IBOutlet UILabel *searchCountLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) id<SearchPresenterInput,SearchInteractorOutput> presenter;
@property (nonatomic,strong) SearchRequest* searchRequest;


@end
