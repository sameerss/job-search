//
//  SearchViewController.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "SearchViewController.h"
#import "Job.h"
#import "JobSearchResultCell.h"

@interface SearchViewController ()

@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (strong, nonatomic) NSMutableArray *jobs;
@property (nonatomic) BOOL isMoreDataAvailable;
@property (nonatomic) BOOL isFetchingData;

@end

@implementation SearchViewController

CGFloat const scrollBuffer = 250;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.searchRequest.searchText;
    
    self.isMoreDataAvailable = YES;
    self.isFetchingData = YES;
    
    self.jobs = [[NSMutableArray alloc] init];
    
    [self.presenter searchWithRequest:self.searchRequest];
    
    [self.searchTableView registerNib:[UINib nibWithNibName:@"JobSearchResultCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JobSearchResultCell"];
    self.searchTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.searchTableView.delegate = self;
    self.searchTableView.dataSource = self;
    self.searchTableView.scrollsToTop = NO;
    self.searchTableView.rowHeight = UITableViewAutomaticDimension;
    self.searchTableView.estimatedRowHeight = 110;
    
    if ([self.searchTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.searchTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [self.searchTableView setHidden:YES];
    [self.activityIndicator setHidden:NO];
    
  }

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JobSearchResultCell *cell;
    
    cell = (JobSearchResultCell *)[tableView dequeueReusableCellWithIdentifier:@"JobSearchResultCell"];
    if(cell == nil) {
     cell = (JobSearchResultCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JobSearchResultCell"];
    }
    
    Job *job = [self.jobs objectAtIndex:indexPath.row];
    cell.jobTitle.text = job.title;
    cell.jobAdvertiser.text = [NSString stringWithFormat:@"%@ on %@",job.advertiser,job.displayListingDate];
    cell.jobSummary.text = job.summary;
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.jobs.count;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat bottom = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if(scrollView.contentOffset.y > bottom - scrollBuffer) {
        if(self.isMoreDataAvailable && !self.isFetchingData) {
            self.isFetchingData = YES;
            self.searchRequest.page++;
            [self.presenter searchWithRequest:self.searchRequest];
        }
    }
}

-(void) receivedSearchResults:(SearchJobResponse *)searchJobResp {
    
    if([self.searchCountLabel.text length] == 0) {
    	self.searchCountLabel.text = [NSString stringWithFormat:@"%d jobs",searchJobResp.jobCount];
    }
    
    [self.jobs addObjectsFromArray:searchJobResp.jobs];
    if(searchJobResp.jobs.count == 0) {
        self.isMoreDataAvailable = NO;
    }
    self.isFetchingData = NO;
    [self.searchTableView setHidden:NO];
    [self.activityIndicator setHidden:YES];
    [self.searchTableView reloadData];
}
@end
