//
//  Utils.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(NSString *)getDateStringFromDate :(NSDate *)date withFormat:(NSString *)dateFormat;
+(NSDate *)getDateFromDateString :(NSString *)dateString withFormat:(NSString *)dateFormat;
+(BOOL) isOnline;
@end
