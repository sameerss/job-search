//
//  TestSearchInteractor.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SearchInteractor.h"

@interface TestSearchInteractor : XCTestCase <SearchInteractorOutput> {
    XCTestExpectation *responeExpectation;
}

@end

@implementation TestSearchInteractor

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSearchInteractor {
    
    responeExpectation = [self expectationWithDescription:@"search interactor"];
    
    SearchJobsRequest *searchJobsReq = [[SearchJobsRequest alloc] initWithKeywords:@"iOS"];
    searchJobsReq.Page = 1;
    
    SearchInteractor *interactor = [[SearchInteractor alloc] init];
    interactor.presenter = self;
    [interactor searchJobs:searchJobsReq];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Server Timeout Error: %@", error);
        }
    }];
    XCTAssert(YES, @"Pass");
}

//Delegate implementation - searchInteractor
-(void) jobSearchResponse:(SearchJobResponse *)searchJobResp {
    [responeExpectation fulfill];
    NSLog(@"COUNT: %d", searchJobResp.jobCount );
    XCTAssertNotNil(searchJobResp,@"json object returned from server is nil");
}

@end
