//
//  seekTests.m
//  seekTests
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SearchViewController.h"
#import "SearchRequest.h"
#import "SearchPresenter.h"
#import "MockSearchInteractor.h"
#import "HttpRequestManager.h"

@interface TestSearchPresenter : XCTestCase <SearchListView> {
    XCTestExpectation *responeExpectation;
}


@end

@implementation TestSearchPresenter

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testSearchPresenter {
    
    MockSearchInteractor *interactor = [[MockSearchInteractor alloc] init];
    SearchPresenter *presenter = [[SearchPresenter alloc] init];
    presenter.jobSearchView = self;
    presenter.interactor = interactor;
    [presenter searchWithRequest:[[SearchRequest alloc] initWithSearchText:@"android"]];
}

- (void)testSearchPresenterNoMock {
    
    responeExpectation = [self expectationWithDescription:@"search presenter"];
    
    SearchInteractor *interactor = [[SearchInteractor alloc] init];
    SearchPresenter *presenter = [[SearchPresenter alloc] init];
    interactor.presenter = presenter;
    presenter.jobSearchView = self;
    presenter.interactor = interactor;
    [presenter searchWithRequest:[[SearchRequest alloc] initWithSearchText:@"android"]];
    
    [self waitForExpectationsWithTimeout:2 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Server Timeout Error: %@", error);
        }
    }];
    XCTAssert(YES, @"Pass");
}

//Delegate implementation - searchPresenter
-(void) receivedSearchResults:(SearchJobResponse *)searchJobResp {
    [responeExpectation fulfill];
    NSLog(@"COUNT: %d", searchJobResp.jobCount );
    XCTAssertNotNil(searchJobResp,@"json object returned from server is nil");
}


@end
