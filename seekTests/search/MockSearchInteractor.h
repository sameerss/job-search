//
//  MockSearchInteractor.h
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchProtocol.h"

@interface MockSearchInteractor : NSObject <SearchInteractorInput>

@property (nonatomic,weak) id<SearchInteractorOutput> presenter;

@end
