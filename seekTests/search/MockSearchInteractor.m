//
//  MockSearchInteractor.m
//  seek
//
//  Created by Sameer Sangawar on 23/04/17.
//  Copyright © 2017 sam. All rights reserved.
//

#import "MockSearchInteractor.h"

@implementation MockSearchInteractor

-(void) searchJobs:(SearchJobsRequest *)searchJobReq {
    SearchJobResponse *searchResponse = [[SearchJobResponse alloc] init];
    [self.presenter jobSearchResponse:searchResponse];
}

@end
